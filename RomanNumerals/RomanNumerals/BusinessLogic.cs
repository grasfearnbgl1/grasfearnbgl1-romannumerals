﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RomanNumerals
{
    public class BusinessLogic
    {
        public string ConvertNumberToRomanNumeral(int numberToConvert)
        {
            if (numberToConvert <= 0)
            {
                return "";
            }
            //I - 1
            //V - 5
            //X - 10
            //L - 50
            //C - 100
            //D - 500

            /*
            70
            highest number that we can deduct from 70 that leaves 0 or higher
            we deduct it and append that character to the string we are returning
            until the remaining number is zero
            */
            Dictionary<int, string> romanNumerals = new Dictionary<int, string>();
            romanNumerals.Add(500, "D");
            romanNumerals.Add(100, "C");
            romanNumerals.Add(50, "L");
            romanNumerals.Add(10, "X");
            romanNumerals.Add(5, "V");
            romanNumerals.Add(1, "I");

            var numeral = string.Empty;
            while (numberToConvert > 0)
            {
                for (int i = 0; i < romanNumerals.Count; i++)
                {
                    var romanNumber = romanNumerals.Keys.ToList()[i];
                    var romanCharacter = romanNumerals.Values.ToList()[i];

                    var multipleOfCurrentRomanNumeral = numberToConvert / romanNumber;

                    if (multipleOfCurrentRomanNumeral > 0)
                    {
                        var previousRomanNumber = romanNumerals.Keys.ToList()[i-1];
                        var previousRomanCharacter = romanNumerals.Values.ToList()[i-1];

                        var remainder = previousRomanNumber  % numberToConvert;

                        if ((remainder) == 1)
                        {
                            numeral = "I" + previousRomanCharacter;
                        }
                        else
                        {
                            for (int x = 1; x <= multipleOfCurrentRomanNumeral; x++)
                            {
                                numeral += romanCharacter;
                            };
                        }

                        numberToConvert = numberToConvert - (multipleOfCurrentRomanNumeral * romanNumber);
                    }
                   
                }
            }

            //var multipleOfFive = numberToConvert / 5;
            //if (multipleOfFive > 0)
            //{
            //    numeral = "V";
            //}
            //var remainder = numberToConvert % 5;
            //if (remainder > 0)
            //{

            //    if ((5 - remainder) == 1 )
            //    {
            //        numeral = "IV";
            //    }
            //    else
            //    {
            //        switch (remainder)
            //        {
            //            case 0:
            //                break;
            //            case 1:
            //                numeral = numeral + "I";
            //                break;
            //            case 2:
            //                numeral = numeral + "II";
            //                break;
            //            case 3:
            //                numeral = numeral + "III";
            //                break;
            //            //case 4:
            //            //    numeral = numeral + "IV";
            //            //    break;
            //        }
            //    }

                
            //}

            return numeral;
        }    
    }
}