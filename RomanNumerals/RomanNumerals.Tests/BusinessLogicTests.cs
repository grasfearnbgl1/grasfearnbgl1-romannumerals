﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanNumerals.Tests
{
    [TestClass]
    public class BusinessLogicTests
    {   
        [TestMethod]
        public void SendOneShouldReceiveI()
        {
            //arrange
            var businessLogic = new BusinessLogic();

            //act
            var numeral = businessLogic.ConvertNumberToRomanNumeral(1);

            //assert
            Assert.AreEqual(numeral, "I");
        }

        [TestMethod]
        public void SendTwoShouldReceiveII()
        {
            //arrange
            var businessLogic = new BusinessLogic();

            //act
            var numeral = businessLogic.ConvertNumberToRomanNumeral(2);

            //assert
            Assert.AreEqual(numeral, "II");
        }

        [TestMethod]
        public void SendTwoShouldReceiveIII()
        {
            //arrange
            var businessLogic = new BusinessLogic();

            //act
            var numeral = businessLogic.ConvertNumberToRomanNumeral(3);

            //assert
            Assert.AreEqual(numeral, "III");
        }

        [TestMethod]
        public void SendFourShouldReceiveIV()
        {
            //arrange
            var businessLogic = new BusinessLogic();

            //act
            var numeral = businessLogic.ConvertNumberToRomanNumeral(4);

            //assert
            Assert.AreEqual(numeral, "IV");
        }

        [TestMethod]
        public void SendFiveShouldReceiveV()
        {
            //arrange
            var businessLogic = new BusinessLogic();

            //act
            var numeral = businessLogic.ConvertNumberToRomanNumeral(5);

            //assert
            Assert.AreEqual(numeral, "V");
        }

        [TestMethod]
        public void SendSixShouldReceiveVI()
        {
            //arrange
            var businessLogic = new BusinessLogic();

            //act
            var numeral = businessLogic.ConvertNumberToRomanNumeral(6);

            //assert
            Assert.AreEqual(numeral, "VI");
        }

        [TestMethod]
        public void SendSevenShouldReceiveVII()
        {
            //arrange
            var businessLogic = new BusinessLogic();

            //act
            var numeral = businessLogic.ConvertNumberToRomanNumeral(7);

            //assert
            Assert.AreEqual(numeral, "VII");
        }

        [TestMethod]
        public void SendEightShouldReceiveVIII()
        {
            //arrange
            var businessLogic = new BusinessLogic();

            //act
            var numeral = businessLogic.ConvertNumberToRomanNumeral(8);

            //assert
            Assert.AreEqual(numeral, "VIII");
        }

        [TestMethod]
        public void SendNineShouldReceiveIX()
        {
            //arrange
            var businessLogic = new BusinessLogic();

            //act
            var numeral = businessLogic.ConvertNumberToRomanNumeral(9);

            //assert
            Assert.AreEqual(numeral, "IX");
        }
    }
}
